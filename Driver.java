import java.util.Scanner;

/*create a herd of rabbit
*enter their stats
*choose what action they want to take when they ancounter another animal
*/
public class Driver {
	public static void main(String[] args) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		System.out.println("You are a herd of rabbits living in a forest.");
		
		//create array of rabbit and enter field for each value
		Rabbit[] rabbitHerd = new Rabbit[4];
		int defense=0;
		int power=0;
		String color="";
		for (int i = 0; i < rabbitHerd.length; i++) {
			
			System.out.println("Enter the color of rabbit #" + (i + 1));
			color=reader.next();
			
			System.out.println("Enter the defense of rabbit #" + (i + 1));
			defense=reader.nextInt();
			
			System.out.println("Enter the power of rabbit #" + (i + 1));
			power=reader.nextInt();
			
			rabbitHerd[i] = new Rabbit(defense,power,color);
			
			System.out.println("");
		}
		System.out.println("Enter the new value of defense for the rabbit #"+(rabbitHerd.length-1 + 1));
		rabbitHerd[rabbitHerd.length-1].setDefense(reader.nextInt());
		
	}
}